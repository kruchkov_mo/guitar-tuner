//
//  SettingDelegate.swift
//  GuitarTuner
//
//  Created by max on 7/25/18.
//  Copyright © 2018 max. All rights reserved.
//

import Foundation

protocol SettingDelegate {
    func onSettingBack(mode: Bool)
}
