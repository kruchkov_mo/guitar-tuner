//
//  FourierUtil.swift
//  GuitarTuner
//
//  Created by max on 7/23/18.
//  Copyright © 2018 max. All rights reserved.
//

import Foundation

class FourierUtil {
    
    static let MIN_HERTZ = 5.0
    static let MAX_HERTZ = 1000.0
    
    static func getNote(_ freq: Double) -> String? {
        let notes = ["e (mi)" : 329.63, "b (si)" : 246.94, "g (sol)" : 196.0, "d (re)" : 146.83, "A (la)" : 110.0, "E (mi)" : 82.41]
        let df = 1.0    // Hz
        for (symbol, val) in notes {
            if fabs(freq - val) <= df {
                return symbol
            }
        }
        return nil
    }
    
    struct ExtremumData {
        var maxIndex: Int
        var minIndex: Int
        var maxValue: Double
        var minValue: Double
    }
    
    static func getMaxAmplitude(_ arr: [Double]) -> ExtremumData {
        var max: Double = arr.first ?? 0.0
        var min: Double = arr.first ?? 0.0
        var maxIndex = 0
        var minIndex = 0
        for i in 0 ..< arr.count {
            let value = arr[i]
            if max < value {
                max = value
                maxIndex = i
            }
            if min > value {
                min = value
                minIndex = i
            }
        }
        return ExtremumData(maxIndex: maxIndex, minIndex: minIndex, maxValue: max, minValue: min)
    }
    
    static func objcFFT(_ arr: [Int16]) -> [Double] {
        let N = arr.count
        let doubleData: [Double] = arr.map{Double($0)}
        
        let a = UnsafeMutablePointer<Double>.allocate(capacity: N)
        a.initialize(to: 0, count: N)
        for i in 0 ..< N {
            a.advanced(by: i).pointee = Double(arr[i])
        }
        defer {
            a.deinitialize(count: N)
            a.deallocate(capacity: N)
        }
        
        let ftData = Util.cpp_fft(a, n: Int32(N), isInverse: false)
        defer {
            ftData?.deinitialize(count: N)
            ftData?.deallocate(capacity: N)
        }
        var result: [Double] = []
        if let d = ftData {
            for i in 0 ..< N {
                result.append((d + i).pointee)
            }
        }
        return result
    }
}
