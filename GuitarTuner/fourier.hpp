//
//  fourier.hpp
//  multi
//
//  Created by max on 7/21/18.
//  Copyright © 2018 max. All rights reserved.
//

#ifndef fourier_hpp
#define fourier_hpp

#include <stdio.h>
#include <cmath>
#include <complex>
#include <vector>
using namespace std;

#define TwoPi 6.283185307179586
typedef complex<double> base;

class Fourier {
    public:
        static double* fft(double *arr, int N, bool isInverse = false);
    
    private:
        static void innerFFT(vector<base> & a, bool invert);
};

#endif /* fourier_hpp */
