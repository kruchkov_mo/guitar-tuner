//
//  Util.h
//  multi
//
//  Created by max on 7/21/18.
//  Copyright © 2018 max. All rights reserved.
//

#ifndef Util_h
#define Util_h

#import <Foundation/Foundation.h>

@interface Util : NSObject

+(double*)cpp_fft:(double*)arr N:(int)n isInverse:(bool)inverse;

@end

#endif /* Util_h */
