//
//  Util.m
//  multi
//
//  Created by max on 7/21/18.
//  Copyright © 2018 max. All rights reserved.
//

#import "Util.h"

#include "fourier.hpp"

@implementation Util

+(double*)cpp_fft:(double*)arr N:(int)n isInverse:(bool)inverse {
    return Fourier::fft(arr, n, inverse);
}

@end
