//
//  FrequencyPlotView.swift
//  multi
//
//  Created by max on 7/19/18.
//  Copyright © 2018 max. All rights reserved.
//

import UIKit

class FrequencyPlotView: UIView {

    let padding: CGFloat = 8.0
    
    var doubleData: [Double]
    let w: CGFloat
    let h: CGFloat
    
    var maxX: CGFloat
    var maxY: CGFloat
    
    var clientWidth: CGFloat {
        get {
            return w - 2*padding
        }
    }
    var clientHeight: CGFloat {
        get {
            return h - 2*padding
        }
    }
    
    init(frame: CGRect, sampleRate: Double) {
        self.doubleData = []
        w = frame.width
        h = frame.height
        maxX = 1.0
        maxY = 1.0
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        drawSpectrum()
    }
    
    func updateData(_ newData: [Double], maxX: CGFloat, maxY: CGFloat) {
        self.doubleData = newData
        self.maxX = maxX
        self.maxY = maxY
        self.setNeedsDisplay()
    }
    
    func drawSpectrum() {
        if self.doubleData.isEmpty {
            return
        }
        guard let ctx = UIGraphicsGetCurrentContext() else { return }
        
        let dx: CGFloat = clientWidth / CGFloat(self.doubleData.count - 1)
        
        let color = UIColor(red: 0.79, green: 0.3, blue: 1.0, alpha: 1.0)
        ctx.setStrokeColor(color.cgColor)
        ctx.setLineWidth(1.6)
        
        ctx.beginPath()
        
        var x0: CGFloat = padding
        var y0: CGFloat = (CGFloat(self.doubleData[0]) * clientHeight ) / maxY
        var x: CGFloat
        var y: CGFloat
        for i in 1 ..< self.doubleData.count {
            y = CGFloat(self.doubleData[i]) * clientHeight / maxY
            x = x0 + dx
            ctx.move(to: CGPoint(x: x0, y: clientHeight - y0))
            ctx.addLine(to: CGPoint(x: x, y: clientHeight - y))
            x0 = x
            y0 = y
        }
        
        ctx.closePath()
        ctx.strokePath()
    }
}
