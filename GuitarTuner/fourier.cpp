//
//  fourier.cpp
//  multi
//
//  Created by max on 7/21/18.
//  Copyright © 2018 max. All rights reserved.
//

#include "fourier.hpp"

void Fourier::innerFFT(vector<base>& a, bool invert) {
    int n = (int) a.size();
    
    for (int i = 1, j = 0; i < n; ++i) {
        int bit = n >> 1;
        for (; j >= bit; bit >>= 1)
            j -= bit;
        j += bit;
        if (i < j)
            swap(a[i], a[j]);
    }
    
    for (int len = 2; len <= n; len <<= 1) {
        double ang = TwoPi/len * (invert ? -1 : 1);
        base wlen(cos(ang), sin(ang));
        for (int i = 0; i < n; i += len) {
            base w (1);
            for (int j = 0; j < len/2; ++j) {
                base u = a[i + j],  v = a[i + j + len/2] * w;
                a[i + j] = u + v;
                a[i + j + len/2] = u - v;
                w *= wlen;
            }
        }
    }
    if (invert)
        for (int i = 0; i < n; ++i)
            a[i] /= n;
}

double* Fourier::fft(double *arr, int N, bool isInverse) {
    vector<base> data;
    int i;
    for(i = 0; i < N; i++) {
        data.push_back(base(arr[i]));
    }
    
    innerFFT(data, isInverse);
    
    double* result = new double[N];
    for (i = 0; i < N; i++) {
        result[i] = sqrt(pow(data[i].real(), 2) + pow(data[i].imag(), 2));
    }
    
    return result;
}
