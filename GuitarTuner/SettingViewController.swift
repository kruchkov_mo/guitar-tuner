//
//  SettingViewController.swift
//  GuitarTuner
//
//  Created by max on 7/25/18.
//  Copyright © 2018 max. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var modeSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationController?.delegate = self
        modeSwitch.isOn = UserDefaults.standard.bool(forKey: "graphicMode")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }*/
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        (viewController as? ViewController)?.isSpectrum = modeSwitch.isOn
        UserDefaults.standard.set(modeSwitch.isOn, forKey: "graphicMode")
    }
}
