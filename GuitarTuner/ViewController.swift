//
//  ViewController.swift
//  GuitarTuner
//
//  Created by max on 7/19/18.
//  Copyright © 2018 max. All rights reserved.
//

import UIKit

import AVFoundation

class ViewController: UIViewController {
    
    let queue = OperationQueue()
    var engine: AVAudioEngine!
    var input: AVAudioInputNode!
    var output: AVAudioOutputNode!
    var format: AVAudioFormat!
    var mixer: AVAudioMixerNode!
    var converter: AVAudioConverter!
    var targetFormat: AVAudioFormat!
    
    @IBOutlet weak var btnSettings: UIBarButtonItem!
    static let SAMPLE_RATE = 8000.0
    static let INTERVAL_NANO_SEC: useconds_t = 20000

    var plot2D: FrequencyPlotView!
    
    var isSpectrum = true
    
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var plotView: UIView!
    
    @IBOutlet weak var frequencyLabel: UILabel!
    
    @IBOutlet weak var btnStart: UIButton!
    
    @IBOutlet weak var btnStop: UIButton!
    
    @IBAction func start(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.1, animations: { sender.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)}, completion: { finish in UIButton.animate(withDuration: 0.1, animations: { sender.transform = CGAffineTransform.identity})})
        
        btnStop.isEnabled = true
        btnStart.isEnabled = false
        startMicListening()
    }
    
    @IBAction func stop(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.1, animations: { sender.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)}, completion: { finish in UIButton.animate(withDuration: 0.1, animations: { sender.transform = CGAffineTransform.identity})})
        btnStop.isEnabled = false
        btnStart.isEnabled = true
        stopMicListening()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnStop.isEnabled = false
        noteLabel.text = ""
        btnStart.layer.cornerRadius = 5
        btnStop.layer.cornerRadius = 5
        plotInit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        btnStop.isEnabled = false
        btnStart.isEnabled = true
        stopMicListening()
    }
    
    func stopMicListening() {
        if engine.isRunning {
            engine.stop()
            queue.cancelAllOperations()
            updateLabel("Frequency: ? Hz")
        }
    }
    
    func audioInit() {
        engine = AVAudioEngine()
        input = engine.inputNode
        format = input.outputFormat(forBus: 0)
        output = engine.outputNode
        mixer = AVAudioMixerNode()
        targetFormat = AVAudioFormat(commonFormat: .pcmFormatInt16, sampleRate: ViewController.SAMPLE_RATE, channels: 1, interleaved: true)
        converter = AVAudioConverter(from: format, to: targetFormat)
    }
    
    func startMicListening() {
        let length = 2048
        audioInit()
        
        engine.attach(mixer)
        engine.connect(input, to: mixer, format: input.outputFormat(forBus: 0))
        engine.connect(mixer, to: output, format: input.inputFormat(forBus: 0))
        
        let ratio: Float = Float(format.sampleRate / targetFormat.sampleRate)
        
        mixer.installTap(onBus: 0, bufferSize: UInt32(length), format: mixer.outputFormat(forBus: 0),
                         block: { [unowned self] (buffer: AVAudioPCMBuffer!, time: AVAudioTime!) -> Void in
                            
                            let capacity = UInt32(Float(buffer.frameCapacity) / ratio)
                            let convertedBuffer = AVAudioPCMBuffer(pcmFormat: self.targetFormat, frameCapacity: capacity)
                            let inputBlock: AVAudioConverterInputBlock = { inNumPackets, outStatus in
                                outStatus.pointee = AVAudioConverterInputStatus.haveData
                                return buffer
                            }
                            var error: NSError? = nil
                            let convertResult = self.converter.convert(to: convertedBuffer!, error: &error, withInputFrom: inputBlock)
 
                            if convertResult == AVAudioConverterOutputStatus.error {
                                print("Convert error has occured")
                            }
                            if let e = error {
                                print("Convert error has occured: [\(e.code)] " + e.description)
                                return
                            }
                            
                            guard let floatBuffer = convertedBuffer?.int16ChannelData else {
                                print("Fail to fetch float array")
                                return
                            }
                            
                            let channelDataValueArray = (0 ..< length).map{ floatBuffer.pointee[$0] }
                            
                            self.queue.addOperation({
                                let maxX: CGFloat
                                let maxY: CGFloat
                                var soundAmplitude: [Double] = []
                                
                                let y = FourierUtil.objcFFT(channelDataValueArray)
                                let extremum = FourierUtil.getMaxAmplitude(y)
                                let frequency = Double(extremum.maxIndex) * ViewController.SAMPLE_RATE / Double(length)
                                if self.isSpectrum {
                                    maxX = CGFloat(y.count - 1) * CGFloat(ViewController.SAMPLE_RATE) / CGFloat(y.count)
                                    maxY = CGFloat(fabs(extremum.maxValue) + fabs(extremum.minValue))
                                } else {
                                    soundAmplitude = (0 ..< Int(length / 10)).map{Double(channelDataValueArray[$0])}
                                    let extremum = FourierUtil.getMaxAmplitude(soundAmplitude)
                                    maxX = 1.0
                                    maxY = CGFloat(fabs(extremum.maxValue) + fabs(extremum.minValue))
                                }
                                DispatchQueue.main.async {
                                    if self.plot2D != nil {
                                        if self.isSpectrum {
                                            self.plot2D.updateData(y, maxX: maxX, maxY: maxY)
                                        } else {
                                            self.plot2D.updateData(soundAmplitude, maxX: maxX, maxY: maxY)
                                        }
                                    }
                                    if FourierUtil.MIN_HERTZ ... FourierUtil.MAX_HERTZ ~= frequency {
                                        let soundNote = FourierUtil.getNote(frequency)
                                        self.updateLabel(String(format:"Frequency: %.2f Hz", frequency))
                                        if let soundNote = soundNote {
                                            self.noteLabel.text = soundNote
                                        }
                                        self.fadeViewInThenOut(view: self.noteLabel, delay: 2.0)
                                    }
                                }
                            })
                            usleep(ViewController.INTERVAL_NANO_SEC)
        })
        engine.prepare()
        do {
            try engine.start()
        } catch {
            print("Error while audio engine starting")
        }
    }
    
    func fadeViewInThenOut(view : UIView, delay: TimeInterval) {
        let animationDuration = 0.25
        UIView.animate(withDuration: animationDuration, animations: { () -> Void in view.alpha = 1})
        { (Bool) -> Void in
            UIView.animate(withDuration: animationDuration, delay: delay, options: .curveEaseInOut, animations: { () -> Void in view.alpha = 0 }, completion: nil)
        }
    }
    
    public func updateLabel(_ newLabelText: String) {
        self.frequencyLabel.text = newLabelText
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        btnSettings.isEnabled = false
        btnSettings.isEnabled = true
    }
    
    fileprivate func plotInit() {
        let rect = CGRect(x: 10, y: 10, width: plotView.frame.size.width - 10, height: plotView.frame.size.height - 10)
        self.plot2D = FrequencyPlotView(frame: rect, sampleRate: ViewController.SAMPLE_RATE)
        self.plot2D.backgroundColor = UIColor(red: 217/255, green: 1.0, blue: 204/255, alpha: 1.0)
        plotView.addSubview(self.plot2D)
        
        self.plot2D.translatesAutoresizingMaskIntoConstraints = false
        let constraint1 = NSLayoutConstraint(item: plot2D,
                                             attribute: NSLayoutAttribute.leading,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: plotView,
                                             attribute: NSLayoutAttribute.leading,
                                             multiplier: 1,
                                             constant: 0)
        let constraint2 = NSLayoutConstraint(item: plot2D,
                                             attribute: NSLayoutAttribute.top,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: plotView,
                                             attribute: NSLayoutAttribute.top,
                                             multiplier: 1,
                                             constant: 0)
        
        
        let constraint3 = NSLayoutConstraint(item: plot2D,
                                             attribute: NSLayoutAttribute.trailing,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: plotView,
                                             attribute: NSLayoutAttribute.trailing,
                                             multiplier: 1,
                                             constant: 0)
        
        let constraint4 = NSLayoutConstraint(item: plot2D,
                                             attribute: NSLayoutAttribute.bottom,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: plotView,
                                             attribute: NSLayoutAttribute.bottom,
                                             multiplier: 1,
                                             constant: 0)
        
        NSLayoutConstraint.activate([constraint1, constraint2, constraint3, constraint4])
    }
}

